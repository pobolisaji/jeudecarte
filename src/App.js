import logo from './logo.svg';
import './App.css';
import {Carte} from './carte';
import {Cartea} from './cartea';
import {Carteb} from './carteb';
import food from './images/food.jpg';
import mer from './images/mer.jpg';
import mojito from './images/mojito.jpg';
import lavande from './images/lavande.jpg';





function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        
        <p> Bonne chance </p>
        <div className="nom">
          <Carte value={1} color="pink" imgsrc={lavande}/>
          <Carte value={2} color="green" imgsrc={food}/>
          <Carte value={3} color="blue" imgsrc={mer}/>
          <Carte value={4} color="yellow" imgsrc={mojito}/>
          <Carte value={5} color="purple" imgsrc={lavande}/>
          <Carte value={6} color="red" imgsrc={mer}/>
          <Carte value={7} color="grey" imgsrc={mojito}/>
          <Carte value={8} color="orange" imgsrc={food}/>
          
        </div>
        <Cartea/>
        <Carteb/>

      </header>
    </div>
    
  );
}

export default App;



