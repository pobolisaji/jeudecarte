import { useState } from 'react';
import './carte.css';



export function Carte(props) {
    const [switcheur, setSwitch] = useState("active");

    

    function handlerSwitch(e) {
        console.log(switcheur);
        if (switcheur === "active") {
            setSwitch("inactive");
        }
        else {
            setSwitch("active");
        }
    }

    return (
        <div className={`Carte ${switcheur}`} onClick={handlerSwitch} >
            <div>
                <div class="recto" style={{backgroundImage:`url(${props.imgsrc})` }}></div>
                <div class="verso" style={{background:props.color}}>{props.value}</div>
            </div>

        </div>


    );
}
    











